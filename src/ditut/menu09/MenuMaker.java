/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2020 Gary F. Pollice
 *******************************************************************************/

package ditut.menu09;

import com.google.inject.Inject;
import ditut.menu09.annotation.*;
import ditut.menu09.service.Menu;

/**
 * MenuMaker with the menu injected.
 * @version Jun 19, 2020
 */
public class MenuMaker
{
	@Inject
	@StandardDiet
	private Menu mainMenu;
	
	@Inject
	@VegetarianDiet
	private Menu secondaryMenu;
	
	public MenuMaker()
	{
		// Intentionally empty
	}
	
	
	public Menu getMainMenu()
	{ 
		return mainMenu;
	}
	
	public Menu getSecondaryMenu()
	{
		return secondaryMenu;
	}
}
