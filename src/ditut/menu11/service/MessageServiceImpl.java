/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2020 Gary F. Pollice
 *******************************************************************************/

package ditut.menu11.service;

import com.google.inject.Singleton;

/**
 * This class is another service that delivers welcome messages.
 * @version Jul 24, 2020
 */
@Singleton
public class MessageServiceImpl implements MessageService
{
	public MessageServiceImpl()
	{
		// Intentionally left empty
	}
	
	/*
	 * @see ditut.menu11.service.MessageService#getWelcomeMessage()
	 */
	@Override
	public String getWelcomeMessage()
	{
		return "Welcome to MenuMaker";
	}
}
