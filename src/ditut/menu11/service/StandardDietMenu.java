/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2020 Gary F. Pollice
 *******************************************************************************/

package ditut.menu11.service;

import com.google.inject.Inject;
import ditut.menu11.annotation.MessageServiceAnnotation;

/**
 * The standard menu. 
 * @version Jun 19, 2020
 */
public class StandardDietMenu implements Menu
{
	@Inject
	@MessageServiceAnnotation
	private MessageService messageService;
	
	/*
	 * @see ditut.menu03.Menu#getMainCourse()
	 */
	@Override
	public String getMainCourse() { return "Grilled Shrimp: ..."; }
	
	/*
	 * @see ditut.menu03.Menu#getSides()
	 */
	@Override
	public String getSides() { return "Sauteed snow peas: ..."; }
	
	/*
	 * @see ditut.menu03.Menu#getAppetizer()
	 */
	@Override
	public String getAppetizer() { return "Stuffed olives: ..."; }
	
	/*
	 * @see ditut.menu03.Menu#getDessert()
	 */
	@Override
	public String getDessert() { return "Pecan pie: ..."; }
	
	/*
	 * @see ditut.menu03.Menu#getBeverages()
	 */
	@Override
	public String getBeverages() { return "Pink lemonade: ..."; }

	/*
	 * @see ditut.menu11.service.Menu#getMessageService()
	 */
	@Override
	public MessageService getMessageService() { return messageService; }
}
