/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2020 Gary F. Pollice
 *******************************************************************************/

package ditut.menu10.annotation;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.*;
import com.google.inject.BindingAnnotation;

/**
 * A BindingAnnotation for the Standard Diet. Any injection that uses
 * this annotation will be injected with the class associated with the 
 * binding in the module that uses .annotatedWith(StandardDiet.class)
 * @version Jun 23, 2020
 */
@BindingAnnotation
@Retention(RUNTIME)
@Target({
	FIELD, METHOD, PARAMETER, CONSTRUCTOR
})
public @interface StandardDiet {}
