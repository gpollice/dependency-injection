/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2020 Gary F. Pollice
 *******************************************************************************/

package ditut.menu10.service;

/**
 * A menu for people on a vegetarian diet.
 * @version Jun 19, 2020
 */
public class VegetarianDietMenu implements Menu
{
	/*
	 * @see ditut.menu03.Menu#getMainCourse()
	 */
	@Override
	public String getMainCourse() { return "Penne pasta and white beans  : ..."; }
	
	/*
	 * @see ditut.menu03.Menu#getSides()
	 */
	@Override
	public String getSides() { return "Artichoke heart salad: ..."; }
	
	/*
	 * @see ditut.menu03.Menu#getAppetizer()
	 */
	@Override
	public String getAppetizer() { return "Stuffed olives: ..."; }
	
	/*
	 * @see ditut.menu03.Menu#getDessert()
	 */
	@Override
	public String getDessert() { return "Tropical fruit plate: ..."; }
	
	/*
	 * @see ditut.menu03.Menu#getBeverages()
	 */
	@Override
	public String getBeverages() { return "Iced lemongrass tea: ..."; }
}
