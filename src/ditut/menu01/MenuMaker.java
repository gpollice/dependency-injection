/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2020 Gary F. Pollice
 *******************************************************************************/

package ditut.menu01;

/**
 * The main class.
 * @version Jun 19, 2020
 */
public class MenuMaker
{
    private final Menu theMenu;
    
    public MenuMaker()
    {
        theMenu = new Menu();
    }
    
    public Menu getMenu()
    {
        return theMenu;
    }
    
    public static void main(String[] args)
    {
        Menu menu = new MenuMaker().getMenu();
        System.out.println("Appetizer\n\t" + menu.getAppetizer());
        System.out.println("Main course\n\t" + menu.getMainCourse());
        System.out.println("Sides\n\t" + menu.getSides());
        System.out.println("Dessert\n\t" + menu.getDessert());
        System.out.println("Beverages\n\t" + menu.getBeverages());
    }
}
