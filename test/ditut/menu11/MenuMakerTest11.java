/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2020 Gary F. Pollice
 *******************************************************************************/

package ditut.menu11;

import static org.junit.Assert.*;
import org.junit.jupiter.api.Test;
import com.google.inject.*;
import com.google.inject.name.Names;
import ditut.menu11.annotation.*;
import ditut.menu11.service.*;

/**
 * Version 10 test cases.
 * @version Jun 19, 2020
 */
class MenuMakerTest11
{
	@Test
	void menuMakerTest()
	{
		Injector injector = Guice.createInjector(new TwoMenuModule());
		MenuMaker mm = injector.getInstance(MenuMaker.class);
		Menu m = mm.getMainMenu();
		assertTrue(m.getAppetizer().contains("olives"));
		Menu m1 = mm.getSecondaryMenu();
		assertTrue(m1.getDessert().contains("fruit"));
		assertEquals("Welcome to MenuMaker", 
			m1.getMessageService().getWelcomeMessage());
		assertTrue(m.getMessageService() == m1.getMessageService());
	}
}

// Binding module.
class TwoMenuModule extends AbstractModule
{
	@Override
	protected void configure()
	{
		bind(Menu.class).annotatedWith(StandardDiet.class)
			.to(StandardDietMenu.class);
		bind(Menu.class).annotatedWith(VegetarianDiet.class)
			.to(VegetarianDietMenu.class);
		bind(MessageService.class).annotatedWith(MessageServiceAnnotation.class)
			.to(MessageServiceImpl.class);
	}
}
