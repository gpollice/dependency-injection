/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2016 Gary F. Pollice
 *******************************************************************************/

package ditut.menu01;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 * Description
 * @version Jun 19, 2020
 */
class MenuMakerTest01
{
	@Test
	void test()
	{
        MenuMaker mm = new MenuMaker();
        Menu menu = mm.getMenu();
        assertNotNull(menu.getAppetizer());
        assertNotNull(menu.getMainCourse());
        assertNotNull(menu.getSides());
        assertNotNull(menu.getDessert());
        assertNotNull(menu.getBeverages());
	}

}
