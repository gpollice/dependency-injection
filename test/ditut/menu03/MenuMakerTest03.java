/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design. The course was
 * taken at Worcester Polytechnic Institute. All rights reserved. This program and the
 * accompanying materials are made available under the terms of the Eclipse Public License
 * v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html Copyright ©2020 Gary F. Pollice
 *******************************************************************************/

package ditut.menu03;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

/**
 * MenuMaker test cases.
 * 
 * @version Jun 19, 2020
 */
class MenuMakerTest03
{
    @ParameterizedTest
    @MethodSource("menuProvider")
    void menuMakerTest(Menu menu)
    {
        MenuMaker mm = new MenuMaker(new StandardDietMenu()); // <-- Client injects the
                                                              // menu
        Menu mmMenu = mm.getMenu();
        assertNotNull(mmMenu.getAppetizer());
        assertNotNull(mmMenu.getMainCourse());
        assertNotNull(mmMenu.getSides());
        assertNotNull(mmMenu.getDessert());
        assertNotNull(mmMenu.getBeverages());
    }

    static Stream<Arguments> menuProvider()
    {
        return Stream.of(Arguments.of(new StandardDietMenu()),
            Arguments.of(new VegetarianDietMenu()));
    }
}
