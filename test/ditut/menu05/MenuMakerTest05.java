/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design. The course was
 * taken at Worcester Polytechnic Institute. All rights reserved. This program and the
 * accompanying materials are made available under the terms of the Eclipse Public License
 * v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html Copyright ©2016 Gary F. Pollice
 *******************************************************************************/

package ditut.menu05;

import static org.junit.Assert.*;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import com.google.inject.*;
import ditut.menu05.service.*;

/**
 * Description
 * 
 * @version Jun 19, 2020
 */
class MenuMakerTest05
{
    @ParameterizedTest
    @MethodSource("menuProvider")
    void menuMakerTest(Injector injector)
    {
        MenuMaker mm = injector.getInstance(MenuMaker.class); // <-- Create the
                                                              // appropriate MenuMaker
        Menu mmMenu = mm.getMenu();
        assertNotNull(mmMenu.getAppetizer());
        assertNotNull(mmMenu.getMainCourse());
        assertNotNull(mmMenu.getSides());
        assertNotNull(mmMenu.getDessert());
        assertNotNull(mmMenu.getBeverages());
    }

    static Stream<Arguments> menuProvider()
    {
        return Stream.of(
            Arguments.of(Guice.createInjector(new StandardDietModule())),
            Arguments.of(Guice.createInjector(new VegetarianDietModule())));
    }
}

// Binding module classes.
class StandardDietModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        bind(Menu.class).to(StandardDietMenu.class);
    }
}

class VegetarianDietModule extends AbstractModule
{
    @Provides
    Menu providesMenu(VegetarianDietMenu menu)
    {
        return menu;
    }
}
