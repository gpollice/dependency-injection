# Dependency Injection Tutorial and Examples using Google Guice
This Eclipse project contains a basic tutorial of how to use a dependency injection framework like Google Guice in different ways to create flexible applications using dependency injection. Before starting this tutorial, you should be familiar with core Java and the concept of dependency injection. This project was designed for use in my CS4233 Object-oriented Analysis & Design course at Worcester Polytechnic Institute.

## Overview
Dependency injection, also referred to as *inversion of control (IoC)*, is fundamental to many, if not most, software systems today. Sometimes it is explicit and implemented by using design patterns like the Factory pattern. Other times developers use a dependency injection framework. For Java, frameworks are based upon [JSR 330](https://jcp.org/en/jsr/detail?id=330). Several frameworks are available such as [Dagger](https://dagger.dev/), [Weld](http://weld.cdi-spec.org/), [Spring](https://spring.io/), [Google Guice](https://github.com/google/guice), and others. The two that seem to be most popular are Spring and Guice; both are freely available. This tutorial uses Google Guice, which I think is quite a bit simpler to use than Spring, mainly due to it's focus on dependency injection rather than the much larger scope of the Spring ecosystem.

You can use Guice several different ways depending upon your needs. Many tutorials and documents are available to help you understand Guice. However, depending upon your familiarity with Java, design patterns, and principles, it may be difficult to get through the jargon. I have tried to make this tutorial sufficient for my course and hopefully more easily accessible to the general audience than some of the other tutorials.

The example(s) used for this tutorial are variations on a simple program that delivers menus to the user. The basic scenario is that the user will ask for a menu for the day and receive the menu that contains instructions for how to make the main course, appetizer, etc. We will not have a complete application, but work our way through some of the variations that we might encounter and how to use techniques, including dependency injection to make our application more flexible and configurable.

### Code structure
We go through several versions of the menu maker application. All of these are rooted in the **ditut** package. This root package contains sub-packages for each version, starting with **menu01** and ending with **menu10**.  Everything under these packages forms the complete code base for a section in the tutorial.

There are two source folders for the project, **src** and **test**. For all versions there is a corresponding version package under **test** that corresponds to the code in the same named package in **src**. The unit tests for each version are in a test file for that version in the **test** folder. The tests are not extensive, but designed to show the dependency injection works.

All of the libraries that you need for this tutorial are in the **lib** folder. Google keeps changing some of its library contents and you need to make sure that the libraries are complete. If you use *maven*, *gradle*, or other dependency management and build automation tools, you should get a consistent set of libraries. I downloaded the libraries manually using the [Maven Central Repository](https://mvnrepository.com/repos/central) and the [Sonatype Maven Search facility](https://search.maven.org/) to determine what the appropriate dependent libraries were. However, these failed to include the *failureaccess-1.0.1.jar* which is necessary to create the Injector instances, and I have included it in the build path.

## Versions
The following sections describe each of the versions. The versions build upon one another. If you already know the material that is introduced in a version, feel free to skip exploring that version. You can always return to it if you missed something.

### Version 01: a simple start
This version corresponds to a quick prototype that a novice programmer might write in a beginning Java programming class. It simply illustrates a proof-of-concept showing that the main logic of getting a menu and printing out its contents. There is a single test, just to make sure that the menu actually returns something other than null results.

### Version 02: simple constructor injection
The previous version tightly coupled the *Menu* and the *MenuMaker*. We gave the *MenuMaker* the responsibility of creating the *Menu*. We want to decouple this if possible make the *MenuMaker* more cohesive and separate concerns to follow the [Single Responsibility Principle (SRP)](https://en.wikipedia.org/wiki/Single-responsibility_principle). This is not a big change. Someone has to create the menu, but we'll defer this until later. For now our test will create the *Menu* and pass it as an argument to the *MenuMake*r constructor.

*MenuMaker* is not meant to be a stand-alone program. It is a component that might be used in other programs; therefore, we remove the main method in the **MenuMaker.java** file.

### Version 03: different menu types
If we're actually going to make our menu application useful we need more than one menu to provide variety to our diet. There are different types of variability that we might want to introduce. The first is variety of menu types. The client application will be more useful if it can cater to different types of diets like vegetarian, lo-carb, and so on. A second type of variability is how the menus are presented; for example as links to recipes on websites. The types and combinations of variabilities can be quite large. In this tutorial we'll use two different menu types for a "standard" and vegetarian diet. If you understand how to use the dependency injection with these, you should be able to do the same with others.

In this version, we extract an interface for the Menu and create two different types. Our test creates one of each type of menu to ensure that the injection works. We use a [parameterized test](https://junit.org/junit5/docs/current/user-guide/#writing-tests-parameterized-tests) to avoid code duplication.

### Version 04: using a factory object to produce menus
This version is a slight modification of version 03. Instead of giving the client (our test) the responsibility of constructing the Menu instance, we delegate it to a factory object using the [Factory design pattern](https://dzone.com/articles/the-factory-design-pattern). The factory object, *MenuFactory*, is the single point of knowledge about how to create the different menus. With only two menus, this is not a big deal, but as the system evolves there may be many types of menus that might require complex code to access sources for the menus. Once again we may be faced with exponential growth in complexity. Isolating the creation of class instances into a factory object is one of the most widely used design patterns.

In order to ensure that no client goes around using the factory to create objects, we apply a [Factory Method pattern](https://dzone.com/articles/java-the-factory-pattern) in the various Menu implementations and make their constructors private. Now, even if a client doesn't use the factory object to get the Menu instances, the only way they can create one is by using the static factory method.

### Version 05: constructor dependency injection with Guice
Finally, we get to the point where we'll start using the Guice dependency injection framework. Don't get too excited. With the simple example that we have thus far, using the framework is probably more trouble than it's worth. The factory approach of the previous version is certainly sufficient and easy to understand. However, we'll start using the framework and then, after seeing a couple of ways to use the dependency injection features, we will expand the example where you can see the power of a framework like Guice.

For this version we have reorganized the code base to explicitly show the menu service. Think of this as a service that delivers menus to clients; in our case, the *MenuMaker* is the client that has a dependency on the *Menu* interface and needs to have a specific instance of the *Menu* delivered to it in order to do its job.

We continue to inject the specific *Menu* instance in the *MenuMaker* through the constructor. The only change we need to make to **MenuMaker.java** is to add the annotation *@Inject* to the *MenuMaker()* constructor. This indicates that the argument(s) to the constructor will be injected from Guice. You may wonder how does it know which type of Menu implementation to use; this is where *bindings* come in. Bindings are at the heart of dependency injection frameworks like Guice. A binding is how we map a (request for) an instance of a given type to the specific class that you want to fulfill that request or contract. There are many types of bindings that Guice supports. We will not go into all of them in this tutorial, just the more common ones. You can find out about all of them in the [Guice User's Guide](https://github.com/google/guice/wiki/Bindings).

In order to create the bindings, we create binding modules which are instances of the Guice *AbstractModule*. You may have one or more binding modules and organize them as you see fit. For our needs right now, we will create one binding module for each of the menus we want to use. If we want to use the standard diet menu instead of the vegetarian menu, we simply use the appropriate binding module.

In this version we override the *configure()* method or add *@Provide* methods to the binding module. We override *configure()* and use the *bind()* method to bind *Menu* to *StandardDietMenu* in the *StandardDietModule*. The *VegetarianDietModule* uses a *@Provide* method. We then use a module to create an *Injector* that we then use to create the client class with the appropriate dependencies bound to it. The two approaches to binding that we used for illustration are identical in this example, but the *@Provides* approach allows for adding additional code to the injected object if you need to do that. 

All of the code for performing the binding can be done by the client. In our example, we perform the binding in the **MenuMakerTest.java** file. The two binding modules are separate classes, shown here, are at the end of the file.

```
class StandardDietModule extends AbstractModule
{
	@Override
	protected void configure()
	{
		bind(Menu.class).to(StandardDietMenu.class);
	}
}

class VegetarianDietModule extends AbstractModule
{
	@Provides
	Menu providesMenu(VegetarianDietMenu menu)
	{
		return menu;
	}
}
```
We use these to create the *Injector* that we pass to the parameterized test as an argument. This is done in the *MenuProvider()* method.

```
	static Stream<Arguments> menuProvider()
	{
		return Stream.of(
			Arguments.of(Guice.createInjector(new StandardDietModule())), 
			Arguments.of(Guice.createInjector(new VegetarianDietModule()))
		);
	}
```
Each test simply asks the *Injector* to get the appropriately provisioned instance of the *MenuMaker* class. Guice creates a graph of all dependencies, so it knows where dependencies are to be injected and how to create them.

```
	@ParameterizedTest
	@MethodSource("menuProvider")
	void menuMakerTest(Injector injector)
	{
		MenuMaker mm = injector.getInstance(MenuMaker.class);	  // <-- Create the appropriate MenuMaker
		Menu mmMenu = mm.getMenu();
		// Rest of the test
	}
```

### Version 06: Field dependency injection
If there is just one argument or there are a couple of arguments that we might pass to a constructor, constructor dependency injection works fine. What if a class has many dependencies? We may not want to pass all of the dependent objects as arguments to the constructor. We may inject appropriate dependent instances into a class using field injection. We use the same binding modules as in the previous example and make two simple changes to the **MenuMaker.java** file.

1. We remove the argument to the *MenuMaker()* constructor and the code in the constructor leaving it as an empty default constructor.
2. We move the *@Inject* annotation to the *theMenu* instance variable.

This is the new **MenuMaker** class:

```
public class MenuMaker
{
	@Inject
	private Menu theMenu;
	
	public MenuMaker()
	{
		// Intentionally empty
	}
	
	public Menu getMenu()
	{ 
		return theMenu;
	}
}
```

You may remove the *MenuMaker()* constructor, but I prefer to leave the empty default there as a way of indicating that I did not forget to include a constructor.

### Version 07: Method dependency injection to set the *Menu*
We've already seen how to set the specified *Menu* instance in our example using the constructor injection (Version 05) and field injection (Version 06). We're going to look at one more approach using a method in the *MenuMaker* class. This is almost identical to constructor injection except that we use a different method, such as a setter. This is a common approach when you are using [JavaBeans](https://www.geeksforgeeks.org/javabean-class-java/). Each field in the JavaBean has a getter and a setter. But we use getters and setters for fields in other types of Java classes as well. Our *MenuMaker* actually fits the definition of a JavaBean if we add a setter method.

For this version, I've added the setter method and moved the dependency injection to this method and removed it from the instance variable. We can use the same binding modules as we have in previous versions. 

```
	@Inject
	public void setMenu(Menu theMenu)
	{
		this.theMenu = theMenu;
	}
```
I have also made a change to the test. Up to this point, we're just checking to make sure that we have some instance of a *Menu*, but we haven't checked to see if it's the right one. We have just tested for a non-null string. I've made a slight modification to the tests to check to see if the main course is the one we're looking for. The test and provider function now look like this:

```
	@ParameterizedTest
	@MethodSource("menuProvider")
	void menuMakerTest(Injector injector, String expectedMainCourse)
	{
		MenuMaker mm = injector.getInstance(MenuMaker.class);	// <-- Create the appropriate MenuMaker
		Menu mmMenu = mm.getMenu();
		assertNotNull(mmMenu.getAppetizer());
		assertTrue(mmMenu.getMainCourse().contains(expectedMainCourse));
		assertNotNull(mmMenu.getSides());
		assertNotNull(mmMenu.getDessert());
		assertNotNull(mmMenu.getBeverages());
		mm.setMenu(new VegetarianDietMenu());	// <-- Change the menu (no injection)
		assertTrue(mm.getMenu().getSides().contains("Artichoke"));
	}
	
	static Stream<Arguments> menuProvider()
	{
		return Stream.of(
			Arguments.of(Guice.createInjector(new StandardDietModule()), "Shrimp"), 
			Arguments.of(Guice.createInjector(new VegetarianDietModule()), "Penne")
		);
	}
```

Notice the last two statements in the *menuMakerTest()* method. These set the *menu* instance variable to the *VegetarionDietMenu* and then verifies that this change occurred. This illustrates that the dependency injection happened only when the *MenuMaker* instance was created. Using the dependency injection as we've done thus far means that all injections occur when the instance is created using the *Injector* from our binding module, and once it has been created everything works as specified without additional injections.

### Version 08: Multiple menus using *@Named* bindings
In the previous versions we needed just one *Menu* instance, but what if we require two different types of menus in our *MenuMaker*? The binding modules as we've written them only allow for one type of *Menu* implementation.

Consider these changes to the **MenuMaker.java** file:

```
public class MenuMaker
{
	@Inject
	@Named("standard")
	private Menu mainMenu;
	
	@Inject
	@Named("vegetarian")
	private Menu secondaryMenu;
	
	public MenuMaker()
	{
		// Intentionally empty
	}
	
	public Menu getMainMenu()
	{ 
		return mainMenu;
	}
	
	public Menu getSecondaryMenu()
	{
		return secondaryMenu;
	}
}
```
Here we have a main menu and a secondary menu instance, both are implementations of *Menu*. We cannot just use an *@Inject* annotation. If we want different types of menus in the two instance variables, we must provide additional information. In this example we use a *@Named* binding annotation in conjunction with the *@Inject* annotation. The *@Named* binding annotation takes a string modifier that is used in our binding module.

```
class MenuMakerTest08
{
	@Test
	void menuMakerTest()
	{
		Injector injector = Guice.createInjector(new TwoMenuModule());
		MenuMaker mm = injector.getInstance(MenuMaker.class);
		Menu m = mm.getMainMenu();
		assertTrue(m.getAppetizer().contains("olives"));
		m = mm.getSecondaryMenu();
		assertTrue(m.getDessert().contains("fruit"));
	}
}

// Binding module.
class TwoMenuModule extends AbstractModule
{
	@Override
	protected void configure()
	{
		bind(Menu.class).annotatedWith(Names.named("standard"))
			.to(StandardDietMenu.class);
		bind(Menu.class).annotatedWith(Names.named("vegetarian"))
			.to(VegetarianDietMenu.class);
	}
}
```
We invoke two *bind()* methods in our binding module, one for each type of *Menu* implementation that we want. We use a functional style to chain methods together. We invoke *annotatedWith()* to identify the classes that we bind with the named instances. The test checks that the instances are injected properly.

### Version 09: Multiple menus using custom binding annotations
Using *@Named* binding annotations as in the previous version works just fine. However, due to the use of a *String* parameter in the *annotatedWith()* method, the compiler cannot check it for accuracy or type correctness. A recommended way is to use a custom binding notation. You can do this two ways: use the *@BindingAnnotation* annotation or a *@Qualifier* annotation. These are equivalent; the first is defined only in Guice and the second is defined in the Java EE **javax.inject** package in the Java libraries. In this version we show an example of each. We create a new package, **ditut.menu09.annotation** to store our binding annotations. Two files go in there, but these could be put in one file or included any other file. The two binding annotations are shown here:

```
// Standard diet binding annotation
@BindingAnnotation
@Retention(RUNTIME)
@Target({
	FIELD, METHOD, PARAMETER, CONSTRUCTOR
})
public @interface StandardDiet {}

// Vegetarian diet binding annotation
@Qualifier @Retention(RUNTIME) @Target({FIELD, METHOD, PARAMETER, CONSTRUCTOR})
public @interface VegetarianDiet {}
```

Notice that in order to create a binding annotation you may need to specify a couple of other things. The [@Target](https://docs.oracle.com/javaee/7/api/javax/enterprise/deploy/spi/Target.html) annotation is used to restrict where the annotation can be applied. [@Retention](https://docs.oracle.com/javase/9/docs/api/java/lang/annotation/Retention.html) indicates how long the annotation is retained. We want it during runtime so we use that annotation. The default is [CLASS retention policy](https://docs.oracle.com/javase/9/docs/api/java/lang/annotation/RetentionPolicy.html).

To use these annotations, we simply replace the *@Named(...)* annotations with *@binding_annotation_name*:

```
	@Inject
	@StandardDiet
	private Menu mainMenu;
	
	@Inject
	@VegetarianDiet
	private Menu secondaryMenu;
```
and modify the binding module to replace the named string with the annotation class:

```
class TwoMenuModule extends AbstractModule
{
	@Override
	protected void configure()
	{
		bind(Menu.class).annotatedWith(StandardDiet.class)
			.to(StandardDietMenu.class);
		bind(Menu.class).annotatedWith(VegetarianDiet.class)
			.to(VegetarianDietMenu.class);
	}
}
```

### Version 10: Using instance bindings for constants
So far, we have used bindings that that bind a type to its implementation. The implementation is created by the Guice framework. What if we want a specific instance of a class or a constant value (e.g. a specific string or int)? For this we would use an instance binding. We identify the injection point as in previous versions and use an instance (constant) binding.

The code for this version adds a instance variable, *welcomeMessage*, to the *MenuMaker* with a getter method:

```
	@Inject
	@Named("Welcome")
	private String welcomeMessage;
	...
	public String getWelcomeMessage()
	{
		return welcomeMessage;
	}
```
and adds a new binding to the binding module:

```
		bind(String.class).annotatedWith(Names.named("Welcome"))
			.toInstance("Welcome to MenuMaker");
```
Notice that the binding uses *toInstance()* instead of *to()*.

### Version 11: Using scope to instantiate a singleton

In the previous version we injected a specific welcome message the *MenuMaker* using field dependency injection. We might prefer to have a service that would provide a random welcome message when we call a particular method. We might want to use this method in several places in our application and would like to only have one instance, that is, a singleton. We can do this with Guice using "[Scopes](https://www.tutorialspoint.com/guice/guice_scopes.htm)." This allows us to reuse a single instance of a class instead of creating multiple instances. You should understand the *Singleton* design pattern. There are different views on the benefits of the Singleton, but if you decide that you want a Singleton, you can use the Scope feature of Guice.

In order to demonstrate the Scope, Version 11 of our code is modified in the following way:
* We added a new binding annotation called *MessageServiceAnnotation*.
* We created a *MessageService* interface and an implementation, *MessageServiceImpl*. There is one method, *getWelcomeMessage()*.
* Each of the menus, standard and vegetarian, has a *MessageService* instance variable. When we get one of the menus from the *MenuMaker* we can ask it for the message service. This is not how we might actually design this, but for our purpose it lets us get the service object from each of the menus and see if they are the same object.

We can simply use the *@Singleton* annotation on the *MessageServiceImpl* class. This looks like this:

```
public class MessageServiceImpl implements MessageService
{
	...
	@Override
	public String getWelcomeMessage()
	{
		return "Welcome to MenuMaker";
	}
}
```

We use the same field injection technique as we did in Version 6.

The test for this version now looks like this:

```
class MenuMakerTest11
{
	@Test
	void menuMakerTest()
	{
		Injector injector = Guice.createInjector(new TwoMenuModule());
		MenuMaker mm = injector.getInstance(MenuMaker.class);
		Menu m = mm.getMainMenu();
		assertTrue(m.getAppetizer().contains("olives"));
		Menu m1 = mm.getSecondaryMenu();
		assertTrue(m1.getDessert().contains("fruit"));
		assertEquals("Welcome to MenuMaker", 
			m1.getMessageService().getWelcomeMessage());
		assertTrue(m.getMessageService() == m1.getMessageService());	// SINGLETON TEST
	}
}

// Binding module.
class TwoMenuModule extends AbstractModule
{
	@Override
	protected void configure()
	{
		bind(Menu.class).annotatedWith(StandardDiet.class)
			.to(StandardDietMenu.class);
		bind(Menu.class).annotatedWith(VegetarianDiet.class)
			.to(VegetarianDietMenu.class);
		bind(MessageService.class).annotatedWith(MessageServiceAnnotation.class)
			.to(MessageServiceImpl.class);
	}
}
```





## References / resources
1. [A hands-on session with Google Guice](https://www.freecodecamp.org/news/a-hands-on-session-with-google-guice-5f25ce588774/), Sankalp Bhatia, freeCodeCamp, *https://www.freecodecamp.org/news/a-hands-on-session-with-google-guice-5f25ce588774/*, accessed 20-Jun-2020.
1. [Dependency Injection](https://en.wikipedia.org/wiki/Dependency_injection), Wikipedia, *https://en.wikipedia.org/wiki/Dependency_injection*, accessed 21-Jun-2020.
1. [The Factory design pattern](https://dzone.com/articles/the-factory-design-pattern), DZone, *https://dzone.com/articles/the-factory-design-pattern*, accessed 20-Jun-2020.
1. [Google Guice Dependency Injection Example Tutorial](https://www.journaldev.com/2403/google-guice-dependency-injection-example-tutorial), JournalDev, *https://www.journaldev.com/2403/google-guice-dependency-injection-example-tutorial*, accessed 20-Jun-2020.
1. [Google Guice User Guide](https://github.com/google/guice/wiki/), *https://github.com/google/guice/wiki/*, accessed 18-Jun-2020.
1. [Java: The Factory Method Pattern](https://dzone.com/articles/java-the-factory-pattern), DZone, *https://dzone.com/articles/java-the-factory-pattern*, accessed 20-Jun-2020.
1. [JUnit 5 User Guide](https://junit.org/junit5/docs/current/user-guide/), *https://junit.org/junit5/docs/current/user-guide/*, accessed 19-Jun-2020.
1. [Learn Google Guice](https://www.tutorialspoint.com/guice/index.htm), *https://www.tutorialspoint.com/guice/index.htm*, accessed 18-Jun-2020.

Copyright ©2020 Gary F. Pollice
